//
//  FavViewController.swift
//  tendfy
//
//  Created by Sergio Fresneda on 09/12/15.
//  Copyright © 2015 Sergio Fresneda. All rights reserved.
//

import UIKit
import CoreData

let reuseIdentifier = "collCell"
var favourites = [NSManagedObject]()

class FavViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 5.0, bottom: 10.0, right: 5.0)
    
    
    /*
     * Arrays para rellenar el contenido de la BDD pra pruebas.
     */
    let names = ["SALÓN TACÓN PIEL PLATAFORMA","SANDALIA TACÓN PIEL ACORDONADA","SALÓN TACÓN PULSERAS","BOTÍN TACÓN PIEL"]
    let latitude = [39.4739846,39.46821,39.4724335,39.4724335]
    let longitude = [-0.3768643,-0.3774087,-0.3796728,-0.3796728]
    let realPrice = [49.95,59.95,39.95,79.95]
    let discountPrice = [39.95,39.95,34.95,69.95]
    let splashPhoto = ["pin1.jpg", "pin2.jpg", "pin3.jpg", "pin4.jpg"]
    let characteristics = ["Sed semper purus eu mi bibendum viverra.\nDonec id blandit lectus.\nMauris pharetra condimentum metus."]
    let descriptionFav = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nulla mauris."]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
         * Bucle de inserción de cuatro productos en la BDD para pruebas
         */
        for index in 0...3{
            insertInToCoreData(index)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func insertInToCoreData(index: NSInteger){
        do{
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            let fetchRequest = NSFetchRequest(entityName: "Favs")
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)
            
            if fetchedResults.count < 4{
                let entity = NSEntityDescription.entityForName("Favs", inManagedObjectContext: managedContext)
                let favs = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                
                favs.setValue(names[index], forKey: "favName");
                favs.setValue(splashPhoto[index], forKey: "favPhotos");
                favs.setValue(splashPhoto[index], forKey: "favSplashPhoto");
                favs.setValue(latitude[index], forKey: "favLatitude");
                favs.setValue(longitude[index], forKey: "favLongitude");
                favs.setValue(realPrice[index], forKey: "favRealPrice");
                favs.setValue(discountPrice[index], forKey: "favDiscountPrice");
                favs.setValue(descriptionFav[0], forKey: "favDescription");
                favs.setValue(characteristics[0], forKey: "favCharacteristics");
                
                do{
                    try managedContext.save()
                    favourites.append(favs)
                    //print("Fav Saved-->\(favs)");
                }catch let error as NSError {
                    print("Ha habido un error \(error), \(error.userInfo)");
                }
                
            }else{
                //print("Eh no!")
            }
        }catch let error as NSError{
            print(error)
        }
        
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        do{
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest(entityName: "Favs")
            
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)
            //print("COUNT!-->\(fetchedResults.count)")
            return fetchedResults.count
            
        }catch{
            print("error!")
        }
        return 0
    }
    
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! FavCollectionViewCell
        do
        {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest(entityName: "Favs")
            
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)
            if fetchedResults.count != 0{
                
                let single_result = fetchedResults[indexPath.row]
                
                let realPrice: Double = (single_result.valueForKey("favRealPrice") as? Double)!
                let discountPrice: Double = (single_result.valueForKey("favDiscountPrice") as? Double)!
                let latitude: Double = (single_result.valueForKey("favLatitude") as? Double)!
                let longitude: Double = (single_result.valueForKey("favLongitude") as? Double)!
                
                let imgName = single_result.valueForKey("favSplashPhoto") as? String
                
                cell.title.text = single_result.valueForKey("favName") as? String
                cell.realPrice.text = String("\(realPrice)€")
                cell.price.text = String("\(discountPrice)€")
                
                cell.pinImage.image = UIImage(named: imgName!)
                cell.distance.text = "\(latitude),\(longitude)"
            }
        }
        catch
        {
            print("Error on load user info")
        }
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: 230, height: 280)
    }
    
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
    
}

