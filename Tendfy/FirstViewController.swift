//
//  FirstViewController.swift
//  tendfy
//
//  Created by Sergio Fresneda on 30/11/15.
//  Copyright © 2015 Sergio Fresneda. All rights reserved.
//

import UIKit
import CoreData

class FirstViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var people = [NSManagedObject]()
    var pickerValue: Int = 0
    let pickerData = ["Woman", "Man"]
    

    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEdad: UITextField!
    @IBOutlet var txtSize: UITextField!
    
    @IBOutlet weak var pkrSex: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.pkrSex.dataSource = self
        self.pkrSex.delegate = self
        
        loadProfileData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveInfoProfile(sender: AnyObject) {
        do{
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            let fetchRequest = NSFetchRequest(entityName: "User")
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)

            
            if fetchedResults.count == 0{
                insertProfileData(managedContext)
            }else{
                updateProfileData(managedContext)
            }
        }catch let error as NSError{
            print(error)
        }
    }
    
    func insertProfileData(managedContext: NSManagedObjectContext){
        
        let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: managedContext)
        let user = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        let pickerValue = Int()

        
        user.setValue(txtName.text, forKey: "name");
        user.setValue(Int(txtEdad.text!), forKey: "edad");
        user.setValue(pickerValue, forKey: "sex");
        user.setValue(Int(txtSize.text!), forKey: "size");
        
        do{
            try managedContext.save()
            people.append(user)
            print(user);
        }catch let error as NSError {
            print("Ha habido un error \(error), \(error.userInfo)");
        }
        
    }
    
    func updateProfileData(managedContext: NSManagedObjectContext){
        do{
            let fetchRequest = NSFetchRequest(entityName: "User")
            let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: managedContext)
            
            fetchRequest.entity = entity
            
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)
            let user = fetchedResults[0]
            
            print(pickerValue);
            
            (user as! User).name = txtName.text
            (user as! User).edad = Int(txtEdad.text!)
            (user as! User).sex = pickerValue
            (user as! User).size = Int(txtSize.text!)

            do{
                try managedContext.save()
                people.append(fetchedResults[0] as! NSManagedObject)
                print(user);
            }catch let error as NSError {
                print("Ha habido un error \(error), \(error.userInfo)");
            }
            
        }
        catch
        {
            print("Error on update")
        }
    }
    
    
    
    func loadProfileData(){
        do
        {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest(entityName: "User")
            
            let fetchedResults = try managedContext.executeFetchRequest(fetchRequest)
            if fetchedResults.count != 0{
                
                let single_result = fetchedResults[0]
                let edadValue: NSInteger = (single_result.valueForKey("edad") as? NSInteger)!
                let sizeValue: NSInteger = (single_result.valueForKey("size") as? NSInteger)!
                let sexValue: NSInteger = (single_result.valueForKey("sex") as? NSInteger)!
                
                pickerValue = sexValue
                txtName.text = single_result.valueForKey("name") as? String
                txtEdad.text = String(edadValue)
                pkrSex.selectRow(sexValue, inComponent: 0, animated: true)
                txtSize.text = String(sizeValue)
                
            }
        }
        catch
        {
            print("Error on load user info")
        }
    }
    
    
    //MARK -Delgates and DataSource
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print("COUNT---> \(pickerData.count)")
        return pickerData.count
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("ROW---> \(pickerData[row])")
        return pickerData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        print("changeRow\(row)")
        pickerValue = row

    }

}









