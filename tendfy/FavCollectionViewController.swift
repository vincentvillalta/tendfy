//
//  FavCollectionViewController.swift
//  tendfy
//
//  Created by Sergio Fresneda on 09/12/15.
//  Copyright © 2015 Sergio Fresneda. All rights reserved.
//



import UIKit

class FavCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var realPrice: UILabel!
    @IBOutlet weak var pinImage: UIImageView!
}