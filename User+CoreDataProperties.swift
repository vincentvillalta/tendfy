//
//  User+CoreDataProperties.swift
//  tendfy
//
//  Created by Sergio Fresneda on 02/12/15.
//  Copyright © 2015 Sergio Fresneda. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var edad: NSNumber?
    @NSManaged var name: String?
    @NSManaged var sex: NSNumber?
    @NSManaged var size: NSNumber?

}
